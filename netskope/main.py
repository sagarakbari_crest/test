"""Netskope Plugin implementation to push and pull the data from Netskope Tenant."""

import requests
import time
import datetime
import re
from netskope.integrations.cte.plugin_base import (
    PluginBase,
    ValidationResult,
    PushResult,
)
from typing import List
from netskope.integrations.cte.models import Indicator, IndicatorType

REGEX_FOR_MD5 = r"^[0-9a-fA-F]{32}$"
REGEX_FOR_SHA256 = r"^[0-9a-fA-F]{64}$"
REGEX_FOR_URL = r"^(\*.?)?(https?:\/\/)?[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*\.[a-zA-Z]+(\/[\w]*)*$"


class NetskopePlugin(PluginBase):
    """NetskopePlugin class having concrete implementation for pulling and pushing threat information."""

    def handle_error(self, resp):
        """Handle the different HTTP response code.

        Args:
            resp (requests.models.Response): Response object returned from API call.
        Returns:
            dict: Returns the dictionary of response JSON when the response code is 200.
        Raises:
            HTTPError: When the response code is not 200.
        """
        if resp.status_code == 200:
            self.logger.info("Plugin: Netskope, Received exit code 200")
            try:
                return resp.json()
            except ValueError:
                self.notifier.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                    f"Exception occurred while parsing JSON response."
                )
                self.logger.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                    f"Exception occurred while parsing JSON response."
                )
        elif resp.status_code == 401:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code 401, Authentication Error"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code 401, Authentication Error"
            )
        elif resp.status_code == 403:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code 403, Forbidden User"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code 403, Forbidden User"
            )
        elif resp.status_code >= 400 and resp.status_code < 500:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code {resp.status_code}, HTTP client Error"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code {resp.status_code}, HTTP client Error"
            )
        elif resp.status_code >= 500 and resp.status_code < 600:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code {resp.status_code}, HTTP server Error"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code {resp.status_code}, HTTP server Error"
            )
        else:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code {resp.status_code}, HTTP Error"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Received exit code {resp.status_code}, HTTP Error"
            )
        resp.raise_for_status()

    def convert_epoch_to_datetime(self, time_stamp):
        """Return the datetime.datetime object from the epoch time.

        Args:
            time_stamp (str, int): Epoch time in string or integer format.
        Returns:
            datetime.datetime: If the time_stap is not None than time_stamp in datetime object
            otherwise current time in datetime object.
        """
        if time_stamp:
            try:
                if type(time_stamp) == str:
                    return datetime.datetime.fromtimestamp(float(time_stamp))
                elif type(time_stamp) in [int, float]:
                    return datetime.datetime.fromtimestamp(time_stamp)
                else:
                    self.notifier.warn(
                        "Plugin Netskope - {} resp.status_code: Invalid timestamp {}, "
                        "Setting current system time {} as firstseen".format(
                            self.configuration["tenant_name"],
                            time_stamp,
                            str(datetime.datetime.now()),
                        )
                    )
                    self.logger.info(
                        "Plugin Netskope - {} Invalid timestamp {}, "
                        "Setting current system time {} as firstseen".format(
                            self.configuration["tenant_name"],
                            time_stamp,
                            str(datetime.datetime.now()),
                        )
                    )
                    return datetime.datetime.now()
            except Exception:
                self.notifier.warn(
                    "Plugin Netskope - {}: Exception occurred while parsing timestamp "
                    "Invalid timestamp {} Setting current system time {} as firstseen".format(
                        self.configuration["tenant_name"],
                        time_stamp,
                        str(datetime.datetime.now()),
                    )
                )
                self.logger.error(
                    "Plugin Netskope - {}: Exception occurred while parsing timestamp "
                    "Invalid timestamp {} Setting current system time {} as firstseen".format(
                        self.configuration["tenant_name"],
                        time_stamp,
                        str(datetime.datetime.now()),
                    )
                )
                return datetime.datetime.now()
        else:
            self.notifier.info(
                f"Plugin Netskope - {self.configuration['tenant_name']}: No timestamp received."
            )
            self.logger.info(
                f"Plugin Netskope - {self.configuration['tenant_name']}: No timestamp received."
            )
            return datetime.datetime.now()

    def get_indicators_from_json(self, json_data, ioc_type):
        """Create the cte.models.Indicator object from the JSON object.

        Args:
            json_data (dict): Indicator received from Netskope.
            ioc_type (str): Type of IOC fetched. malware or malsite.
        Returns:
            cte.models.Indicator: Indicator object from the dictionary.
        """
        indicator_list = []
        tenant_url = f"https://{self.configuration['tenant_name']}.goskope.com"
        comment_str = tenant_url
        for threat in json_data:
            if ioc_type == "malware":
                comment_str = f"{comment_str} - {threat.get('object','')}"
                indicator_list.append(
                    Indicator(
                        value=threat["local_md5"],
                        type=IndicatorType.MD5,
                        firstSeen=self.convert_epoch_to_datetime(
                            threat.get("timestamp")
                        ),
                        lastSeen=self.convert_epoch_to_datetime(
                            threat.get("timestamp")
                        ),
                        comments=comment_str,
                    )
                )
            else:
                malsite_category = ", ".join(
                    threat.get("malsite_category", [])
                )
                comment_str = f"{comment_str} - {malsite_category}"
                indicator_list.append(
                    Indicator(
                        value=threat["url"],
                        type=IndicatorType.URL,
                        firstSeen=self.convert_epoch_to_datetime(
                            threat.get("timestamp")
                        ),
                        lastSeen=self.convert_epoch_to_datetime(
                            threat.get("timestamp")
                        ),
                        comments=comment_str,
                    )
                )
            comment_str = tenant_url
        return indicator_list

    def fetch_threat_data(
        self, endpoint_url, api_token, start_time, end_time, ioc_type
    ):
        """Fetch the Threat information from Netskope Tenant API.

        Args:
            endpoint_url (str): Endpoint URl from where Threat information is fetched.
            api_token (str): Netskope API token for Authentication.
            start_time (datetime.datetime): Start time from when Threat information to be fetched.
            end_time (epoch time): Endtime till when Threat information to be fetched.
            ioc_type (str): Type of Threat data to fetch. Malware or Malsite.
        Returns:
            List[cte.models.Indicator]: List of malsite or malware indicators fetched from Netskope.
        Raises:
            requests.HTTPError: When HTTP response code is not 200 or some error occurred in the API call.
        """
        skip = 0
        if start_time is None:
            start_time = datetime.datetime.now() - datetime.timedelta(
                days=int(self.configuration.get("days", 7))
            )

        req_params = {
            "type": ioc_type,
            "token": api_token,
            "starttime": start_time.timestamp(),
            "endtime": end_time,
            "limit": 5000,
            "skip": skip,
        }
        headers = {"User-Agent": f"cte-{self.configuration['tenant_name']}"}
        indicator_list = []
        while True:
            try:
                resp = requests.get(
                    endpoint_url,
                    params=req_params,
                    headers=headers,
                    verify=self.ssl_validation,
                    proxies=self.proxy,
                )
            except requests.exceptions.ProxyError:
                self.notifier.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Invalid proxy configuration."
                )
                self.logger.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Invalid proxy configuration."
                )
                raise requests.HTTPError(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Invalid proxy configuration."
                )
            except requests.exceptions.ConnectionError:
                self.notifier.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Invalid proxy server provided."
                )
                self.logger.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Invalid proxy server provided."
                )
                raise requests.HTTPError(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Invalid proxy server provided."
                )
            except requests.exceptions.RequestException as e:
                self.notifier.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Exception occurred while making an API call to Netskope"
                )
                self.logger.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Exception occurred while making an API call to Netskope"
                )
                raise e
            # Handle HTTP error codes
            json_resp = self.handle_error(resp)
            status = json_resp.get("status")
            if status == "error":
                raise requests.HTTPError(" ".join(json_resp["errors"]))
            elif status == "success":
                if len(json_resp["data"]) < 5000:
                    indicator_list.extend(
                        self.get_indicators_from_json(
                            json_resp["data"], ioc_type
                        )
                    )
                    break
                else:
                    req_params["skip"] = req_params["skip"] + 5000
                indicator_list.extend(
                    self.get_indicators_from_json(json_resp["data"], ioc_type)
                )
            else:
                self.notifier.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Unexpected Error occurred while fetching data from Netskope"
                )
                raise requests.HTTPError(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} "
                    f"Unexpected Error occurred while fetching data from Netskope"
                )
        return indicator_list

    def split_indicator_list(self, indicators, max_hash_size, max_url_size):
        """Split up indicator list in 2 parts based on the indicator type.

        Args:
            indicators (List[cte.models.Indicator]): Indicator list to be splitted.
            max_hash_size (int): Max hash file size in characters.
            max_url_size (int): Max url file size in characters.
        Returns:
            tuple (List[cte.models.Indicator], List[cte.models.Indicator]): Indicator list splitted in 2 parts.
            First one is Malsite Indicators, Second one is Malware indicators.
        """
        malware_indicators = []
        malware_size = 0
        malware_exceeded = False
        malsite_indicators = []
        malsite_size = 0
        malsite_exceeded = False
        # Split the indicator list in 2 parts according to the type
        for indicator in indicators:
            if not malware_exceeded and (
                indicator.type == IndicatorType.MD5
                or indicator.type == IndicatorType.SHA256
            ):
                if (malware_size + len(indicator.value) + 1) <= max_hash_size:
                    malware_indicators.append(indicator.value)
                    malware_size += len(indicator.value) + 1
                else:
                    malware_exceeded = True
            elif not malsite_exceeded:
                if (malsite_size + len(indicator.value) + 1) <= max_url_size:
                    malsite_indicators.append(indicator.value)
                    malsite_size += len(indicator.value) + 1
                else:
                    malsite_exceeded = True
        if malware_exceeded:
            self.notifier.info(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Exceeded payload size limit for malware indicators. "
                f"Skipping rest of the indicators."
            )
        if malsite_exceeded:
            self.notifier.info(
                f"Plugin: Netskope - {self.configuration['tenant_name']}, "
                f"Exceeded payload size limit for malsite indicators. "
                f"Skipping rest of the indicators."
            )
        return malsite_indicators, malware_indicators

    def add_dummy_indicator(
        self, indicators, threat_data_type, default_file_hash, default_url
    ):
        """Add dummy indicators to indicator list if required.

        Args:
            indicators (List[cte.models.Indicator]): List of Malware and Malsite Indicator objects.
            threat_data_type (str): The threat data type. it can be Malware, URL and Both.
            default_file_hash (str): The default value of file hash.
            default_url (str): The default value of url.
        """
        indicators = [i for i in indicators]
        if threat_data_type == "Both" or threat_data_type == "Malware":
            # Search if 'Malware' is present or not.
            malware_present = False
            for indicator in indicators:
                if (
                    indicator.type == IndicatorType.MD5
                    or indicator.type == IndicatorType.SHA256
                ):
                    malware_present = True
                    break
            if not malware_present:
                # Check default_file_hash value
                if re.compile(REGEX_FOR_MD5).match(default_file_hash):
                    indicators.append(
                        Indicator(
                            value=default_file_hash, type=IndicatorType.MD5
                        )
                    )
                else:
                    indicators.append(
                        Indicator(
                            value=default_file_hash, type=IndicatorType.SHA256
                        )
                    )

        if threat_data_type == "Both" or threat_data_type == "URL":
            # Search if 'URL' is present or not.
            url_present = False
            for indicator in indicators:
                if indicator.type == IndicatorType.URL:
                    url_present = True
                    break
            if not url_present:
                indicators.append(
                    Indicator(
                        value=default_url,
                        type=IndicatorType.URL,
                        firstSeen=datetime.datetime.now(),
                        lastSeen=datetime.datetime.now(),
                        comment="This is test IOC.",
                    )
                )
        return indicators

    def push_threat_data(
        self,
        api_url,
        api_token,
        file_hash_list_name,
        url_list_name,
        indicators: List[Indicator],
    ):
        """Pushed threat to the Netskope Tenant file and URL list.

        Args:
            api_url (str): API URL in the form of https://<tenant-name>.goskope.com/api/v1
            api_token (str): API token for Authentication with Netskope API.
            file_hash_list_name (str): File hash list where file hashes of Malware Indicators to be Pushed.
            url_list_name (str): URL list name where the URL of Malsite Indicators to be pushed.
            indicators (List[cte.models.Indicator]): List of Malware and Malsite Indicator objects
            to be pushed on Netskope.
        Returns:
            cte.plugin_base.PushResult: PushResult object with success or failure boolean and message.
        """
        json_data_offset = 30
        (
            final_malsite_payload,
            final_malware_payload,
        ) = self.split_indicator_list(
            indicators,
            (self.configuration["max_file_hash_cap"] * 10 ** 6)
            - json_data_offset
            - len(file_hash_list_name),
            (self.configuration["max_url_list_cap"] * 10 ** 6)
            - json_data_offset
            - len(url_list_name),
        )
        push_malwares = True
        push_malsites = True
        file_hash_list_endpoint = "{}/updateFileHashList".format(api_url)
        url_list_endpoint = "{}/updateUrlList".format(api_url)
        if (
            file_hash_list_endpoint in self.storage
            and url_list_endpoint in self.storage
        ):  # not the first time
            new_malware_hash = hash(",".join(sorted(final_malware_payload)))
            push_malwares = (
                self.storage[file_hash_list_endpoint] != new_malware_hash
            )
            new_malsite_hash = hash(",".join(sorted(final_malsite_payload)))
            push_malsites = self.storage[url_list_endpoint] != new_malsite_hash
            self.storage[file_hash_list_endpoint] = new_malware_hash
            self.storage[url_list_endpoint] = new_malsite_hash
            if not push_malwares:
                self.logger.info(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} No new malwares to be pushed."
                )
            if not push_malsites:
                self.logger.info(
                    f"Plugin: Netskope - {self.configuration['tenant_name']} No new malsites to be pushed."
                )
        else:  # is first push; both will be pushed
            self.storage[file_hash_list_endpoint] = hash(
                ",".join(sorted(final_malware_payload))
            )
            self.storage[url_list_endpoint] = hash(
                ",".join(sorted(final_malsite_payload))
            )
        req_params = {"token": api_token}
        valid = True
        return_message = ""
        notification_msg = ""
        try:
            if (
                push_malwares
                and self.configuration["threat_data_type"]
                in ["Both", "Malware"]
                and final_malware_payload
            ):
                file_hash_resp = self.post_indicators_to_netskope(
                    file_hash_list_endpoint,
                    req_params,
                    file_hash_list_name,
                    final_malware_payload,
                )
                file_hash_json = self.handle_error(file_hash_resp)
                if file_hash_json["status"] == "error":
                    valid = False
                    return_message = "{} {}".format(
                        "Error while pushing file hash list to Netskope",
                        " ".join(file_hash_json["errors"]),
                    )
                    notification_msg = (
                        "Error while pushing file hash list to Netskope, "
                        "Invalid File Hashes."
                    )
            if (
                push_malsites
                and self.configuration["threat_data_type"] in ["Both", "URL"]
                and final_malsite_payload
            ):
                url_list_resp = self.post_indicators_to_netskope(
                    url_list_endpoint,
                    req_params,
                    url_list_name,
                    final_malsite_payload,
                )
                url_list_json = self.handle_error(url_list_resp)
                if url_list_json["status"] == "error":
                    valid = False
                    return_message = "{} {} {}".format(
                        return_message,
                        "Error while pushing URL list to Netskope",
                        " ".join(url_list_json["errors"]),
                    )
                    notification_msg = (
                        "Error while pushing URL list to Netskope, "
                        "Invalid URLs"
                    )
            if not valid:
                self.notifier.error(
                    f"Plugin: Netskope - {self.configuration['tenant_name']}, {notification_msg}"
                )
                return PushResult(success=False, message=return_message)
        except requests.exceptions.ProxyError:
            self.notifier.error(
                f"Plugin: Netskope -{self.configuration['tenant_name']} "
                f"Invalid proxy configuration."
            )
            self.logger.error(
                f"Plugin: Netskope -{self.configuration['tenant_name']} "
                f"Invalid proxy configuration."
            )
            return PushResult(
                success=False,
                message="Plugin: Netskope Invalid proxy configuration.",
            )
        except requests.exceptions.ConnectionError:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Invalid proxy server provided."
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Invalid proxy server provided."
            )
            return PushResult(
                success=False,
                message="Plugin: Netskope Invalid proxy server provided.",
            )
        except requests.exceptions.RequestException as e:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Exception occurred while making an API call to Netskope"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Exception occurred while making an API call to Netskope"
            )
            return PushResult(
                success=False,
                message=f"Exception occurred while making an API call to Netskope {repr(e)}",
            )
        except Exception as e:
            self.notifier.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Exception occurred while pushing data to Netskope. {repr(e)}"
            )
            self.logger.error(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Exception occurred while pushing data to Netskope. {repr(e)}"
            )
            return PushResult(success=False, message=str(e))
        total_pushed_indicators = len(final_malsite_payload) + len(
            final_malware_payload
        )
        self.logger.info(
            "Plugin: Netskope {} Successfully pushed {} Indicators to the Netskope.".format(
                self.configuration["tenant_name"], total_pushed_indicators
            )
        )
        return PushResult(
            success=True, message="Successfully pushed data to Netskope"
        )

    def post_indicators_to_netskope(
        self, endpoint_url, req_params, container_name, indicator_list
    ):
        """Make a POST API call to the Netskope tenant for pushing Threat data.

        Args:
            endpoint_url (str): Netskope endpoint URL for pushing Threat Information.
            req_params (dict): Query string params for POST request. (dict object having 'api_token')
            container_name (str): Name of file hash list or URL list.
            indicator_list (List[str]): List of Indicator values
        Returns:
            requests.models.Response:
        """
        headers = {"User-Agent": f"cte-{self.configuration['tenant_name']}"}
        payload_data = {
            "name": container_name,
            "list": ",".join(indicator_list),
        }
        return requests.post(
            endpoint_url,
            params=req_params,
            json=payload_data,
            headers=headers,
            verify=self.ssl_validation,
            proxies=self.proxy,
        )

    def pull(self):
        """Pull the Threat information from Netskope Tenant.

        Returns:
            List[cte.models.Indicators]: List of indicator objects received from the Netskope.
        """
        # Let's trip the spaces from the tenant name.
        self.configuration["tenant_name"] = self.configuration[
            "tenant_name"
        ].replace(" ", "")
        config = self.configuration
        if config["is_pull_required"] == "Yes":
            self.logger.info("Polling is enabled.")
            end_time = time.time()
            base_url = "https://{}.goskope.com".format(config["tenant_name"])

            alert_endpoint = "{}/{}".format(base_url, "api/v1/alerts")
            threat_type = config["threat_data_type"]
            malware_indicators = []
            malsite_indicators = []
            if threat_type == "Both" or threat_type == "Malware":
                malware_indicators = self.fetch_threat_data(
                    alert_endpoint,
                    config["api_token"],
                    self.last_run_at,
                    end_time,
                    "malware",
                )
            if threat_type == "Both" or threat_type == "URL":
                malsite_indicators = self.fetch_threat_data(
                    alert_endpoint,
                    config["api_token"],
                    self.last_run_at,
                    end_time,
                    "malsite",
                )

            malware_indicators.extend(malsite_indicators)
            return malware_indicators
        else:
            self.logger.info(
                f"Plugin: Netskope - {self.configuration['tenant_name']} "
                f"Polling is disabled, skipping."
            )
            return []

    def push(self, indicators: List[Indicator]):
        """Push the Indicator list to the Netskope file or URL list.

        Args:
            indicators (List[cte.models.Indicators]): List of Indicator objects to be pushed.
        Returns:
            cte.plugin_base.PushResult: PushResult object with success flag and Push result message.
        """
        self.logger.info(
            f"Plugin: Netskope - {self.configuration['tenant_name']} "
            f"Executing push method for Netskope plugin"
        )
        # Let's trip the spaces from the tenant name.
        self.configuration["tenant_name"] = self.configuration[
            "tenant_name"
        ].replace(" ", "")
        config = self.configuration
        base_url = "https://{}.goskope.com".format(config["tenant_name"])
        api_url = "{}/{}".format(base_url, "api/v1")

        indicators = self.add_dummy_indicator(
            indicators,
            config["threat_data_type"],
            config["default_file_hash"].strip(),
            config["default_url"].strip(),
        )
        return self.push_threat_data(
            api_url,
            config["api_token"],
            config["file_list"],
            config["url_list"],
            indicators,
        )

    def validate(self, data):
        """Validate the Plugin configuration parameters.

        Args:
            data (dict): Dict object having all the Plugin configuration parameters.
        Returns:
            cte.plugin_base.ValidateResult: ValidateResult object with success flag and message.
        """
        self.logger.info(
            "Plugin: Netskope Executing validate method for Netskope plugin"
        )
        if (
            "api_token" not in data
            or not data["api_token"]
            or type(data["api_token"]) != str
        ):
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Type of API token should be string."
            )
            return ValidationResult(
                success=False, message="Invalid API Token provided.",
            )

        if (
            "tenant_name" not in data
            or not data["tenant_name"]
            or type(data["tenant_name"]) != str
        ):
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Type of Tenant name should be non-empty string."
            )
            return ValidationResult(
                success=False, message="Invalid Tenant name provided.",
            )

        if "is_pull_required" not in data or data["is_pull_required"] not in [
            "Yes",
            "No",
        ]:
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Type of Pulling configured should be integer."
            )
            return ValidationResult(
                success=False,
                message="Invalid value for 'Enable Polling' provided. Allowed values are 'Yes', or 'No'.",
            )

        try:
            if (
                "days" not in data
                or not data["days"]
                or int(data["days"]) <= 0
            ):
                self.logger.error(
                    "Plugin: Netskope Validation error occured Error: Invalid days provided."
                )
                return ValidationResult(
                    success=False, message="Invalid Number of days provided.",
                )
        except ValueError:
            return ValidationResult(
                success=False, message="Invalid Number of days provided.",
            )

        if "threat_data_type" not in data or data["threat_data_type"] not in [
            "Both",
            "Malware",
            "URL",
        ]:
            self.logger.error(
                "Plugin: Netskope Invalid value for 'Type of Threat data to pull' provided. "
                "Allowed values are Both, Malware, or URL."
            )
            return ValidationResult(
                success=False,
                message="Invalid value for 'Type of Threat data to pull' provided. "
                "Allowed values are 'Both', 'Malware', or 'URL'.",
            )

        if (
            data["threat_data_type"] == "Both"
            or data["threat_data_type"] == "Malware"
        ) and (
            "file_list" not in data
            or not data["file_list"]
            or type(data["file_list"]) != str
        ):
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Invalid File Hash List name."
            )
            return ValidationResult(
                success=False, message="Invalid File Hash List name.",
            )

        if (
            data["threat_data_type"] == "Both"
            or data["threat_data_type"] == "URL"
        ) and (
            "url_list" not in data
            or not data["url_list"]
            or type(data["url_list"]) != str
        ):
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Invalid URL List name."
            )
            return ValidationResult(
                success=False, message="Invalid URL List name.",
            )

        if "max_file_hash_cap" not in data or type(
            data["max_file_hash_cap"]
        ) not in [int, float]:
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Type of Max file hash count should be integer."
            )
            return ValidationResult(
                success=False,
                message="Invalid value for 'Max File Hash Count' provided.",
            )

        if "max_url_list_cap" not in data or type(
            data["max_url_list_cap"]
        ) not in [int, float]:
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Type of Max URL count to share should be integer."
            )
            return ValidationResult(
                success=False,
                message="Invalid value for 'Max URL Count' provided.",
            )

        if (
            data["threat_data_type"] == "Both"
            or data["threat_data_type"] == "Malware"
        ) and (
            "default_file_hash" not in data
            or not data["default_file_hash"]
            or (
                not re.compile(REGEX_FOR_MD5).match(data["default_file_hash"].strip())
                and not re.compile(REGEX_FOR_SHA256).match(
                    data["default_file_hash"].strip()
                )
            )
        ):
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Invalid Default File Hash."
            )
            return ValidationResult(
                success=False, message="Invalid Default File Hash.",
            )

        if (
            data["threat_data_type"] == "Both"
            or data["threat_data_type"] == "URL"
        ) and (
            "default_url" not in data
            or not data["default_url"]
            or not re.compile(REGEX_FOR_URL).match(data["default_url"].strip())
        ):
            self.logger.error(
                "Plugin: Netskope Validation error occurred for Netskope plugin "
                "Error: Invalid Default URL."
            )
            return ValidationResult(
                success=False, message="Invalid Default URL.",
            )

        return self.validate_auth_params(
            data["tenant_name"], data["api_token"]
        )

    def validate_auth_params(self, tenant_name, api_token):
        """Validate the authentication params with Netskope tenant.

        Args:
            tenant_name (str): Netskope Tenant Name.
            api_token (str): API token for authentication with the mentioned Netskope Tenant.
        Returns:
            ValidationResult: ValidationResult object having validation results after making
            an API call.
        """
        tenant_name = tenant_name.replace(" ", "")
        base_url = "https://{}.goskope.com".format(tenant_name)
        alert_endpoint = "{}/{}".format(base_url, "api/v1/alerts")
        req_params = {
            "type": "malware",
            "token": api_token,
            "starttime": (
                datetime.datetime.now() - datetime.timedelta(days=90)
            ).timestamp(),
            "endtime": datetime.datetime.now().timestamp(),
        }
        headers = {"User-Agent": f"cte-{tenant_name}"}
        try:
            resp = requests.get(
                alert_endpoint,
                params=req_params,
                headers=headers,
                verify=self.ssl_validation,
                proxies=self.proxy,
            )
            json_resp = self.handle_error(resp)
            status = json_resp.get("status")
            if status == "error":
                if json_resp.get("errorCode") == "Authorization Error":
                    self.logger.error(
                        "Plugin: Netskope Validation Error, "
                        "Invalid API token provided"
                    )
                    return ValidationResult(
                        success=False,
                        message="Validation Error, Invalid API Token provided.",
                    )
                else:
                    errors = " ".join(json_resp["errors"])
                    self.logger.error(
                        f"Plugin: Netskope Validation Error, "
                        f"Error while validating credentials {errors}."
                    )
                    return ValidationResult(
                        success=False,
                        message=f"Validation Error, Error in validating Credentials {errors}",
                    )
            elif status == "success":
                self.logger.info(
                    "Plugin: Netskope Validation Successful for Netskope plugin"
                )
                return ValidationResult(
                    success=True,
                    message="Validation Successful for Netskope plugin",
                )
            else:
                self.logger.error(
                    "Plugin: Netskope Validation Error, Error while validating credentials"
                )
                return ValidationResult(
                    success=False,
                    message="Validation Error, Error in validating Credentials",
                )
        except requests.exceptions.ProxyError:
            self.logger.error(
                "Plugin: Netskope Validation Error, "
                "Invalid proxy configuration."
            )
            return ValidationResult(
                success=False,
                message="Validation Error, Invalid proxy configuration.",
            )
        except requests.exceptions.ConnectionError:
            self.logger.error(
                "Plugin: Netskope Validation Error, "
                "Invalid Tenant Name/proxy server provided."
            )
            return ValidationResult(
                success=False,
                message="Validation Error, Invalid Tenant Name/proxy server provided.",
            )
        except requests.HTTPError as err:
            self.logger.error(
                f"Plugin: Netskope Validation Error, "
                f"Error in validating Credentials {repr(err)}"
            )
            return ValidationResult(
                success=False,
                message=f"Validation Error, Error in validating Credentials {repr(err)}",
            )
